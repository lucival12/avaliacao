package br.avaliacao

class Papel {
    String nome
    String codigo
    String descricao

    static constraints = {
        descricao(nullable: true)
    }
}
